#!/usr/bin/env bash

set -e -x

PROGRAMS_PATH="$HOME/programs"
if [ ! -d "$PROGRAMS_PATH" ]; then
    mkdir "$PROGRAMS_PATH"
fi

# Install brew. Add brew rmtree subcommand.
if ! which brew; then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    brew tap beeftornado/rmtree
fi

# Install Sublime Text and Sublime Merge. Enable press-and-hold.
# Add symbolic links to subl and smerge.
SUBLIME_TEXT_PATH='/Applications/Sublime\ Text.app'
if [ -d "$SUBLIME_TEXT_PATH" ]; then
    ln -s /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl "$PROGRAMS_PATH"
    defaults write com.sublimetext.4 ApplePressAndHoldEnabled -bool false
fi
SUBLIME_MERGE_PATH='/Applications/Sublime\ Merge.app'
if [ -d "$SUBLIME_MERGE_PATH" ]; then
    ln -s /Applications/Sublime\ Merge.app/Contents/SharedSupport/bin/smerge "$PROGRAMS_PATH"
fi

# Do not automatically open downloaded files in Preview.
defaults write com.apple.Safari AutoOpenSafeDownloads -bool NO


