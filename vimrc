inoremap df <Esc>

set history=1000
set nowrap
syntax on
set showmatch
set title
set matchtime=2

set autoindent
set smartindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

set nocompatible
set noswapfile
