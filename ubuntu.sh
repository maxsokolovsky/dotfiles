#!/usr/bin/env bash

set -ex

echo 'export PS1="\W \t $ "' >> "/home/$USER/.bashrc"

# General utilities.
sudo apt update && sudo apt install -y git vim curl tree tmux shellcheck htop

# Docker.
sudo apt install -y apt-transport-https ca-certificates software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update && sudo apt install -y docker-ce
sudo usermod -aG docker "${USER}"

# Sublime Text and Sublime Merge.
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg > /dev/null
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt update && sudo apt install -y sublime-text sublime-merge

# kubectl.
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# Vagrant and Terraform.
wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install -y vagrant terraform

# Go.
sudo add-apt-repository -y ppa:longsleep/golang-backports
sudo apt update && sudo apt install -y golang-go

# Set up PATH.
mkdir -p "/home/$USER/go"
echo 'export PATH=$PATH:/home/$USER/go/bin' >> "/home/$USER/.bashrc"

# rbenv and Ruby.
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'eval "$(~/.rbenv/bin/rbenv init - bash)"' >> "/home/$USER/.bashrc"
source "/home/$USER/.bashrc"
git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build
sudo apt-get update && sudo apt-get install -y build-essential libyaml-dev libz-dev libssl-dev
RUBY_VERSION=3.2.2
rbenv install "$RUBY_VERSION" && rbenv global "$RUBY_VERSION"
